# coding=utf-8
import os
import json
from configparser import ConfigParser
from playsound import playsound

from urllib.request import urlopen
from urllib.request import Request
from urllib.error import URLError
from urllib.error import HTTPError
from urllib.parse import urlencode
from urllib.parse import quote_plus

TEXT = "欢迎使用百度语音合成。"
ini_file = "./config.ini"
cfg_name = "default"
book = "D:/总要删的/追风筝的人.txt"


def load_config(ini, name):
    cfg = ConfigParser()
    # 读取文件内容
    cfg.read(ini, encoding="gbk")
    # cfg.items()返回list，元素为tuple
    return dict(cfg.items(name))


class DemoError(Exception):
    pass


def fetch_token(dft_cfg):
    # print("fetch token begin")
    params = {'grant_type': 'client_credentials',
              'client_id': dft_cfg['api_key'],
              'client_secret': dft_cfg['secret_key']}
    post_data = urlencode(params)
    post_data = post_data.encode('utf-8')
    req = Request(dft_cfg['token_url'], post_data)
    try:
        f = urlopen(req, timeout=5)
        result_str = f.read()
    except URLError as err:
        print('token http response http code : ' + str(err.code))
        result_str = err.read()
    result_str = result_str.decode()

    # print(result_str)
    result = json.loads(result_str)
    # print(result)
    if 'access_token' in result.keys() and 'scope' in result.keys():
        if not dft_cfg['scope'] in result['scope'].split(' '):
            raise DemoError('scope is not correct')
        # print('SUCCESS WITH TOKEN: %s ; EXPIRES IN SECONDS: %s' % (result['access_token'], result['expires_in']))
        return result['access_token']
    else:
        raise DemoError('MAYBE API_KEY or SECRET_KEY not correct: access_token or scope not found in token response')


def update_text(file, book_title, ini):
    # 读取配置文件
    cfg = ConfigParser()
    # 读取文件内容
    cfg.read(ini, encoding="gbk")
    if cfg.has_option(book_title, "text_lct"):
        now_lct = int(cfg.get(book_title, "text_lct"))
    else:
        cfg.add_section(book_title)
        now_lct = 0

    if len(file) <= now_lct:
        return "已经读到最后一句啦！换本书吧~！"
    else:
        while not len(file[now_lct].strip()):
            now_lct = now_lct + 1
        # 更新配置文件
        cfg.set(book_title, "text_lct", str(now_lct + 1))
        cfg.write(open(ini, "r+"))
        return file[now_lct]


def request_api(params):
    data = urlencode(params)
    req = Request(dft_cfg['tts_url'], data.encode('utf-8'))
    try:
        f = urlopen(req)
        result_str = f.read()
        headers = dict((name.lower(), value) for name, value in f.headers.items())
        has_error = ('content-type' not in headers.keys() or headers['content-type'].find('audio/') < 0)
    except Exception as e:
        print('asr http response http code : ' + str(e))
        result_str = str(e)
        has_error = True
    if has_error:
        print("tts api  error:" + str(result_str, 'utf-8'))
        request_api(params)
    else:
        # Step 3.4: 保存请求的音频结果并输出成temp.mp3，朗读完毕后删除
        save_file = "error.txt" if has_error else 'temp.' + dft_cfg['format']
        with open(save_file, 'wb') as of:
            of.write(result_str)
        playsound(save_file)
        os.remove(save_file)


if __name__ == '__main__':
    # Step 1: 载入配置文件
    dft_cfg = load_config(ini_file, cfg_name)
    # Step 2: 获取Token
    token = fetch_token(dft_cfg)
    # Step 3: 向API发起请求
    # Step 3.1: 初始化请求参数params、书籍标题
    params = {'tok': token, 'tex': '', 'per': dft_cfg['per'], 'spd': dft_cfg['spd'], 'pit': dft_cfg['pit'],
              'vol': dft_cfg['vol'], 'aue': dft_cfg['aue'], 'cuid': dft_cfg['cuid'],
              'lan': 'zh', 'ctp': 1}  # lan ctp 固定参数
    book_title = (book.split('/'))[-1]
    # 打开指定书籍, 并按行读取
    with open(book, "r", encoding='utf-8') as f:
        file = f.readlines()
    # Step 3.2: 不断获取文本并朗读请求得到的音频
    while 1:
        # Step 3.2.1: 根据上次阅读的位置，更新需要合成的文本内容
        TEXT = update_text(file, book_title, ini_file)
        print(TEXT)
        params['tex'] = quote_plus(TEXT)  # 此处TEXT需要两次urlencode
        # Step 3.2.2: 将参数打包，并向指定URL请求，并朗读
        request_api(params)
