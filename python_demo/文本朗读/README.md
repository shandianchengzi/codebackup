# 使用方式

1. 接入token：在百度AI开放平台创建应用，添加语音合成功能。在`config.ini`中添加自己的`api_key`和`secret_key`。
2. (可略)下载`baidu-aip`：`pip install baidu-aip`。
3. 读入txt文本：修改`main.py`中的book变量，设置为自己的文本路径。
4. 执行python文件：在代码的目录下执行`python3 .\main.py`。

# 使用说明

## config.ini

发音人选择, 基础音库：0为度小美，1为度小宇，3为度逍遥，4为度丫丫，  
精品音库：5为度小娇，103为度米朵，106为度博文，110为度小童，111为度小萌，默认为度小美  
per = 3  
语速，取值0-15，默认为5中语速  
spd = 4  
音调，取值0-15，默认为5中语调  
pit = 5  
音量，取值0-9，默认为5中音量  
vol = 5  
下载的文件格式, 3：mp3(default) 4： pcm-16k 5： pcm-8k 6. wav  
aue = 3  
下载的文件格式, 可选项：mp3(default), pcm-16k, pcm-8k, wav  
format = mp3  

## main.py

`__main__`函数：  
- Step 1: 载入配置文件  
- Step 2: 获取Token  
- Step 3: 向API发起请求  
    - Step 3.1: 初始化请求参数params、书籍标题  
    - Step 3.2: 不断获取文本并朗读请求得到的音频  
        - Step 3.2.1: 根据上次阅读的位置，更新需要合成的文本内容  
        - Step 3.2.2: 将参数打包，并向指定URL请求，并朗读