﻿#include <iostream>
#include <Windows.h>
#include <tchar.h> // 使用 _T宏

using namespace std;

int test()
{
	//_T宏可以把一个引号引起来的字符串，根你的环境设置，使得编译器会根据编译目标环境选择合适的（Unicode还是ANSI）字符处理方式
	LPCTSTR lpSubKey = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");
	HKEY hKey;
	REGSAM flag = KEY_WOW64_64KEY;//当前系统为win7 64位，访问的是64位的注册表，如果访问32位，则改为KEY_WOW64_32KEY
	LONG lRet = RegOpenKeyEx(HKEY_CURRENT_USER, lpSubKey, 0, KEY_ALL_ACCESS | flag, &hKey);
	if (ERROR_SUCCESS != lRet)
	{
		cout << "RegOpenKeyEx fail!" << endl;
		return 0;
	}

	LPCTSTR pchrName = _T("D:\\voice\\voice.exe");
	lRet = RegSetValueEx(hKey, TEXT("SSD"), 0, REG_SZ, (LPBYTE)pchrName, strlen(pchrName) * sizeof(TCHAR) + 1);
	//(hKey, "SSD", 0, REG_SZ, (CONST BYTE*)Data, sizeof(TCHAR) * iLen);

	if (ERROR_SUCCESS != lRet)
	{
		cout << "RegSetValueEx fail!" << endl;
		return 0;
	}
	RegCloseKey(hKey);

	return 0;
}

int main()
{
	test();
	system("pause");
	return 0;
}