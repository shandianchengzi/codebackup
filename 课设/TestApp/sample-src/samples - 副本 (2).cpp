﻿#include <WINSOCK2.H>
#include <stdio.h>
#include <iostream>
#include<string>
#include<io.h>
//#include "package.h"
#pragma comment(lib,"ws2_32.lib")
#define BUFFERLENGTH 4096
using namespace std;

int getBytesLength(char* buf) {
	char len[5];
	for (int i = 1; i < 5; i++) {
		len[i - 1] = buf[i];
	}
	len[4] = '\0';
	return atoi(len);
}
int main(int argc, char** argv)
{
	system("color f0");
	int err;
	WORD versionRequired;
	WSADATA wsaData;
	versionRequired = MAKEWORD(1, 1);
	char str[30];
	int segema = 0;
	err = WSAStartup(versionRequired, &wsaData);//协议库的版本信息
	printf("正在连接服务器！\n");
	char receiveBuf[BUFFERLENGTH + 6];
	char sendBuf[BUFFERLENGTH + 1];


	//while(1){
	SOCKET clientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);//0
	SOCKADDR_IN clientsock_in;
	clientsock_in.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
	clientsock_in.sin_family = AF_INET;
	clientsock_in.sin_port = htons(8888);
	segema = connect(clientSocket, (SOCKADDR*)&clientsock_in, sizeof(SOCKADDR));//开始连接
	if (segema == SOCKET_ERROR)
	{
		cout << "Connect Error::" << GetLastError() << endl;
		return -1;

	}
	else
	{
		cout << " 连接成功 !" << endl;

	}
	memset(receiveBuf, 0x00, sizeof(receiveBuf));
	//cout << "aaaa" << endl;
	segema = recv(clientSocket, receiveBuf, sizeof(receiveBuf), 0);
	//cout << segema << endl;
	if (segema == 0 || segema == SOCKET_ERROR)
	{
		cout << "接收失败！";
	}
	else {
		cout << "接收到：" << receiveBuf << endl;
	}
	segema++;
	while (1) {
		memset(receiveBuf, 0x00, sizeof(receiveBuf));
		printf("-----------------------------\n");
		printf("请输入文件名，输入quit退出\n");
		cin >> str;
		if (strcmp(str, "quit") == 0)
		{
			//break;
			return 0;
		}
		int len = strlen(str) + 1;
		send(clientSocket, str, len, 0);
		printf("请输入文件保存的路径:(输入1默认保存到F:\\VS代码\\FTP客户端\\下载文件)\n");
		string path;
		cin >> path;
		if (path == "1") {
			path = "F:\\VS代码\\FTP客户端\\下载文件";
			//cout << path << endl;
		}
		string spath = path + "\\";
		spath += str;
		//cout << spath << endl;
		FILE* outfp;
		if ((outfp = fopen(spath.c_str(), "w")) == NULL)
		{
			cout << "不能保存该文件：  " << spath << endl;
			continue;
			//exit(2);
		}
		int bytesize = 0;
		while (1)
		{
			int len = recv(clientSocket, receiveBuf, BUFFERLENGTH + 5, 0);
			receiveBuf[BUFFERLENGTH + 5] = '\0';
			if (len > 0)
			{
				int flag = getBytesLength(receiveBuf);
				if (flag > 0)
				{
					if (flag == strlen(receiveBuf) - 5)
					{
						bytesize += flag;
						printf("\b\b\b\b\b\b\b\b\b\\b\b\b\b\b\b\b\b\b\b\b已接收%d% Byte", bytesize);
						for (int i = 5; i < strlen(receiveBuf); i++)
						{
							putc(receiveBuf[i], outfp);
						}
					}
					if (receiveBuf[0] == '0' && flag == strlen(receiveBuf) - 5)
					{
						printf("\n\n文件%s下载完成\n\n\n", str);
						fclose(outfp);
						break;
					}
					if (receiveBuf[0] == '9')
					{
						printf("文件%s不存在!\n", str);
						fclose(outfp);
						remove(spath.c_str());
						//segema = 1;
						break;
					}
				}
			}
		}

	}

	closesocket(clientSocket);


	WSACleanup();
	system("pause");
	return 0;
}