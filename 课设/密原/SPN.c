#include <stdio.h>
#include <string.h>
int pi_s[0x10]= {0xe,0x4,0xd,0x1,0x2,0xf,0xb,0x8,0x3,0xa,0x6,0xc,0x5,0x9,0x0,0x7};
int pi_s2[0x10]= {0xe0,0x40,0xd0,0x10,0x20,0xf0,0xb0,0x80,0x30,0xa0,0x60,0xc0,0x50,0x90,0x00,0x70};
//int pi_s2[0x10][0x10]= {{0xe0},{0x40},{0xd0},{0x10},{0x20},{0xf0},{0xb0},{0x80},{0x30},{0xa0},{0x60},{0xc0},{0x50},{0x90},{0x00},{0x70}};
int pi_s3[0x10]= {0xe00,0x400,0xd00,0x100,0x200,0xf00,0xb00,0x800,0x300,0xa00,0x600,0xc00,0x500,0x900,0x000,0x700};
int pi_s4[0x10]= {0xe000,0x4000,0xd000,0x1000,0x2000,0xf000,0xb000,0x8000,0x3000,0xa000,0x6000,0xc000,0x5000,0x9000,0x0000,0x7000};
//int pi_ss[16] = {0xe, 0x5, 0xf, 0x2, 0x6, 0xa, 0xd, 0xf, 0xb, 0x3, 0xc, 0x7, 0x9, 0x4, 0xe, 0x8};
int pi_s_fan[0x10]= {0xe,0x3,0x4,0x8,0x1,0xc,0xa,0xf,0x7,0xd,0x9,0x6,0xb,0x2,0x0,0x5};
//int pi_s_fan2[0x10]= {0xe0,0x30,0x40,0x80,0x10,0xc0,0xa0,0xf0,0x70,0xd0,0x90,0x60,0xb0,0x20,0x00,0x50};
//int pi_s_fan3[0x10]= {0xe00,0x300,0x400,0x800,0x100,0xc00,0xa00,0xf00,0x700,0xd00,0x900,0x600,0xb00,0x200,0x000,0x500};
//int pi_s_fan4[0x10]= {0xe000,0x3000,0x4000,0x8000,0x1000,0xc000,0xa000,0xf000,0x7000,0xd000,0x9000,0x6000,0xb000,0x2000,0x0000,0x5000};
//int pi_p[16]= {1,5,9,13,2,6,10,14,3,7,11,15,4,8,12,16};
//1:1 6:6 11:11 16:16
//2:5
//3:9
//4:13
//7:10
//8:14
//12:15
//快读
unsigned long read10()
{
    //register意思是将变量放入寄存器，大幅提升效率
    register unsigned long s = 0;
    register char ch = getchar(); // 读入单个字符到寄存器
    //为了避免输入数字之前的空格造成影响以及判断正负
    //while ((ch < '0' || ch>'9') && (ch < 'a' || ch > 'f'))
    //{
    //	ch = getchar();
    //}
    while (ch >= '0' && ch <= '9')
    {
        s = (s << 3) + (s << 1) + ch - '0';
        ch = getchar();
    }
    return s;
}

unsigned long read16()
{
    //register意思是将变量放入寄存器，大幅提升效率
    register unsigned long s = 0;
    register char ch = getchar(); // 读入单个字符到寄存器
    //为了避免输入数字之前的空格造成影响以及判断正负
    //while ((ch < '0' || ch>'9') && (ch < 'a' || ch > 'f'))
    //{
    //	ch = getchar();
    //}
    while ((ch >='a' && ch <='f') || (ch >= '0' && ch <= '9'))
    {
        if(ch >='a' && ch <='f')
        {
            s = (s << 4) + ch - 'a' + 0xa;
        }
        else
        {
            s = (s << 4) + ch - '0';
        }
        ch = getchar();
    }
    return s;
}

void write16_4 (unsigned long s)
{
    int k = 0, len = 4;
    while(len--)
    {
        if(s){
            k = (k << 4) + (s & 0xf);
        }
        else k<<=4;
        s >>=4;
    }
    len=4;
    while(len--)
    {
        if((k&0xf)<0xa)
            putchar((k & 0xf) + '0');
        else
            putchar((k & 0xf) + 'a' - 0xa);
        k >>= 4;
    }
}

// 1. getchar读入(1~3步，1900ms->1600ms)
// 2. 取余和乘除改与运算
// 3. 长整型改4个数组(结果耗时更长了哭了，然后改了回来)
// 4. P盒置换的时候只处理需要被置换的6对数据(1600ms->1300ms)
// 5. 首先单独生成k密钥流，与加密分离，从而简化生成过程。(1300ms->1200ms)
// 6. 将两句printf改成一句printf，再改成putchar。！！！终于过了！！！
int main()
{
    unsigned long s1,s2,k[5],w,u,v,n,y;
    //int ss1[8],ss2[4],ww[4],uu[4],vv[4],k[5][4];
    //scanf("%lu%*c",&n);

    n=read10();
    for(unsigned long i=0; i<n; i++)
    {
        //s1 = s2 = 0;
        s1 = read16();
        s2 = read16();
        //printf("%x%x%x%x %x%x%x%x\n",ss1[7],ss1[6],ss1[5],ss1[4], ss2[3],ss2[2],ss2[1],ss2[0]);
        w=s2;
        register char a = 5;
        while(a--)
        {
            k[a]=s1&0xffff;
            s1>>=4;
        }
        for(int j=0; j<4; j++)
        {
            //k[j] = (s1 >> (16-(j<<0x2)) ) &0xffff;
            u=k[j]^w;
            v = pi_s4[u>>12] | pi_s3[(u>>8)&0xf] | pi_s2[(u>>4)&0xf] | pi_s[u&0xf];  // S盒代换
            if(j!=3)
            {
                w = v;
                if(((v >> 14) & 1) ^ ((v >> 11) & 1))
                    w^=0x4800;
                if(((v >> 13) & 1) ^ ((v >> 7) & 1))
                    w^=0x2080;
                if(((v >> 12) & 1) ^ ((v >> 3) & 1))
                    w^=0x1008;
                if(((v >> 9) & 1) ^ ((v >> 6) & 1))
                    w^=0x240;
                if(((v >> 8) & 1) ^ ((v >> 2) & 1))
                    w^=0x104;
                if(((v >> 4) & 1) ^ ((v >> 1) & 1))
                    w^=0x12;

                //for(int l=0; l<16; l++)
                //{
                //    w = w | ( ( (v>>(15-l))&0x1 ) << (16-pi_p[l]));   // P盒置换
                //}
            }
            else
            {
                //k[j+1]=(s1>> (12-(j<<0x2)) ) &0xffff;
                y = v^k[j+1];
            }
        }

        w = y ^ 0x1;    // 末位取反
        v=k[4]^w;
        u = pi_s_fan[v>>12]<<12 | pi_s_fan[(v>>8)&0xf]<<8 | pi_s_fan[(v>>4)&0xf]<<4 | pi_s_fan[v&0xf];   // S盒逆代换
        //printf("%x\n",w);
        for(int j=3; j>0; j--)
        {
            w=k[j]^u;
            v = w;
            if(((w >> 14) & 1) ^ ((w >> 11) & 1))
                v^=0x4800;
            if(((w >> 13) & 1) ^ ((w >> 7) & 1))
                v^=0x2080;
            if(((w >> 12) & 1) ^ ((w >> 3) & 1))
                v^=0x1008;
            if(((w >> 9) & 1) ^ ((w >> 6) & 1))
                v^=0x240;
            if(((w >> 8) & 1) ^ ((w >> 2) & 1))
                v^=0x104;
            if(((w >> 4) & 1) ^ ((w >> 1) & 1))
                v^=0x12;
            //for(int l=0; l<16; l++)
            //{
            //    v = v | ( ( (w>>(15-l))&0x1 ) << (16-pi_p[l]));   // P盒逆置换 P盒竟然不需要改
            //}
            u = pi_s_fan[v>>12]<<12 | pi_s_fan[(v>>8)&0xf]<<8 | pi_s_fan[(v>>4)&0xf]<<4 | pi_s_fan[v&0xf];   // S盒逆代换
            if(j==1)
            {
                w = k[j-1]^u;
                write16_4(y);
                putchar(' ');
                write16_4(w);
                putchar('\n');
                //printf("%04x %04x\n",y, w);
            }
        }
    }
    return 0;
}
