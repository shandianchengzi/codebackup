#pragma once
#ifndef BASE64_H
#define BASE64_H
#include <string.h>
#include <stdlib.h>
#include <wchar.h> 
#include <assert.h>
#include <windows.h>
#include <iostream>
typedef unsigned char     uint8;
typedef unsigned long    uint32;
uint32 base64_encode(char* input, uint8* encode, int codeSize);
int base64_decode(const uint8* code, uint32 code_len, char* str);
char* G2U(const char* gb2312);
#endif // !BASE64_H
