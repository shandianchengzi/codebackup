#include "DealShm.h"

func_info* InitShm(HANDLE& hSemaphore, HANDLE& hMap, LPVOID& pShm, WCHAR nameSemap[], WCHAR nameShm[])
{
    // 打开或创建信号量
    hSemaphore = OpenSemaphore(EVENT_ALL_ACCESS, FALSE, nameSemap);
    if (hSemaphore == NULL) {
        hSemaphore = CreateSemaphore(NULL, 0, 1, nameSemap);
        if (hSemaphore == NULL) {
            OutputDebugString(L"信号量创建失败！");
            exit(-1);
        }
    }
    // 打开或创建共享内存: Name:"dbgBuf", size:3000h
    hMap = OpenFileMapping(FILE_MAP_ALL_ACCESS, 0, nameShm);
    if (hMap == NULL) {
        hMap = CreateFileMapping(INVALID_HANDLE_VALUE, NULL,
            PAGE_READWRITE | SEC_COMMIT, 0, 0x3000, nameShm);
        if (hMap == NULL) {
            OutputDebugString(L"共享内存创建失败！");
            exit(-1);
        }
    }
    pShm = MapViewOfFile(hMap, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, 0);
    if (pShm == NULL)
    {
        OutputDebugString(L"获取共享内存映像失败！");
        exit(-1);
    }
    // 初始化共享内存内容为0
    memset(pShm, 0, sizeof(func_info));
    return (func_info*)pShm;
}

SHM* InitShm(HANDLE& hMap, LPVOID& pShm, WCHAR nameShm[])
{
    // 打开或创建共享内存: Name:"dbgBuf", size:3000h
    hMap = OpenFileMapping(FILE_MAP_ALL_ACCESS, 0, nameShm);
    if (hMap == NULL) {
        hMap = CreateFileMapping(INVALID_HANDLE_VALUE, NULL,
            PAGE_READWRITE | SEC_COMMIT, 0, 0x3000, nameShm);
        if (hMap == NULL) {
            OutputDebugString(L"共享内存创建失败！");
            exit(-1);
        }
    }
    pShm = MapViewOfFile(hMap, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, 0);
    if (pShm == NULL)
    {
        OutputDebugString(L"获取共享内存映像失败！");
        exit(-1);
    }
    // 初始化共享内存内容为0
    memset(pShm, 0, sizeof(SHM));
    return (SHM*)pShm;
}


// 超过等待最大次数则返回1表示异常
int WaitState(SHM* info, int count, int MaxWaitTimes)
{
    while (info->state[count] && MaxWaitTimes--) { Sleep(50); }
    if (MaxWaitTimes)
        return 0;
    else
        return 1;
}

int WriteShm(HANDLE& hSemaphore, SHM* shm, int count, func_info info)
{
    memcpy_s(&shm->info[count], sizeof(info), &info, sizeof(info));     // 修改内存块信息
    shm->state[count] = 1;                                              // 修改内存块状态为已写入
    ReleaseSemaphore(hSemaphore, 1, NULL);                              // 增加信号量
    count = (count + 1) % shm_num;                                      // 递增内存块序号
    return count;
}

// 更新共享内存处理位置信息
int UpdateShm(HANDLE& hSemaphore, int count)
{
    ReleaseSemaphore(hSemaphore, 1, NULL);
    count = (count + 1) % shm_num;
    return count;
}

int UpdateShm(SHM* shm, int& count)
{
    int i = shm_num;
    while (shm->state[count] && i--) {
        Sleep(50);
        OutputDebugStringA("test");
        count = (count + 1) % shm_num;
    }
    if (!i)return -1;
    return count;
}

//关闭共享内存, 防止内存泄漏
void DestoryShm(HANDLE& hMap, LPVOID& pShm)
{
    if (hMap != nullptr)
    {
        UnmapViewOfFile(pShm);      //解除文件映射
        CloseHandle(hMap);          //关闭句柄
    }
}