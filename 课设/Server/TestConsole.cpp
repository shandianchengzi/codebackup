// TestConsole.cpp: 将DLL注入到目标客户端程序。
//


#include "head.h"
#include "base64.h"




HANDLE ThreadCreate(char ThreadName[], LPTHREAD_START_ROUTINE ThreadFunc, LPVOID lpParam)
{
    DWORD dwThreadId = 0;
    HANDLE hThreadFunc;
    hThreadFunc = CreateThread(NULL	// 默认安全属性
        , NULL		    // 默认堆栈大小
        , ThreadFunc    // 线程入口地址
        , lpParam	    // 传递给线程函数的参数
        , 0		        // 指定线程立即运行
        , &dwThreadId	// 线程ID号
    );
    if (hThreadFunc == NULL) {
        printf_s("%s线程创建失败！\n", ThreadName);
        exit(-1);
    }
    return hThreadFunc;
}
int main()
{
    //setvbuf(stdout, NULL, _IOLBF, sizeof(stdout));
    setlocale(LC_ALL, "chs");       // 保证中文正常输出

    // 开启服务器
    HANDLE hThreadServer = ThreadCreate((char*)"Server", ThreadServer, NULL);
    //std::cout << "欢迎使用Hook测试程序!\n";
    //printf("键入任意符号即可中止程序\n");
    // Create a server endpoint
    getchar();
    CloseHandle(hThreadServer);
    return 0;
}

// 运行程序: Ctrl + F5 或调试 >“开始执行(不调试)”菜单
// 调试程序: F5 或调试 >“开始调试”菜单
