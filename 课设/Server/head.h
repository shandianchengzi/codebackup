#pragma once
#ifndef _HEAD_H_
#define _HEAD_H_

#include <iostream>
#include <cstdio>
#include <windows.h>
#include <string>

#include "Process.h"

#include "psapi.h"
#include "detours.h"
#pragma comment(lib, "detours.lib")


// 共享的变量声明


// 共享的函数声明
// DealError.cpp
void OutputError();

// TestConsole.cpp
HANDLE ThreadCreate(char ThreadName[], LPTHREAD_START_ROUTINE ThreadFunc, LPVOID lpParam);

// ProcessWithDll.cpp: EXE注入
DWORD WINAPI ThreadHook(LPVOID lpParam);
// Server.cpp: 输出信息
DWORD WINAPI ThreadServer(LPVOID lpParam);


#endif
