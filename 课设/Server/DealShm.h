#pragma once
/*
// 共享变量使用前声明
HANDLE hSemap;                      // 信号量句柄
WCHAR nameSemap[10] = L"dbgSemap";  // 信号量名称
HANDLE hMap;                        // 内存映射对象句柄
WCHAR nameShm[10] = L"dbgShm";      // 共享内存名称
LPVOID pShm;                        // 共享内存指针
int count = 0;                      // 当前操作的字符串编号
SHM* shm;                           // 函数信息结构体指针
std::mutex mtx[shm_num];          // 共享内存读写锁
*/

#ifndef DEALSHM_H
#define DEALSHM_H

#include <windows.h>
#include <iostream>
#include <mutex>

#define shm_num 10
#define argValueSize 120
typedef struct shm_data {
    SYSTEMTIME st;                          // Hook处理时间戳
    int argNum;                             // 参数数量
    // argName首项为函数类型序号，argValue首项为函数名称
    char argName[10][30];                   // 参数名称
    char argValue[10][argValueSize];                  // 参数值
}func_info;

typedef struct shm_space {
    int state[shm_num];                      // 如果state是1，则为有信息
    func_info info[shm_num];
}SHM;

// DealShm.cpp: 共享内存
func_info* InitShm(HANDLE& hSemaphore, HANDLE& hMap, LPVOID& pShm, WCHAR nameSemap[], WCHAR nameShm[]);
SHM* InitShm(HANDLE& hMap, LPVOID& pShm, WCHAR nameShm[]);
// 超过等待最大次数则返回1表示异常
int WaitState(SHM* info, int count, int MaxWaitTimes);
// 更新共享内存处理信息
int WriteShm(HANDLE& hSemaphore, SHM* shm, int count, func_info info);
int UpdateShm(HANDLE& hSemaphore, int count);
int UpdateShm(SHM* shm, int& count);
// 关闭共享内存, 防止内存泄漏
void DestoryShm(HANDLE& hMap, LPVOID& pShm);

#endif