#include "head.h"

void OutputError()
{
    LPVOID lpBuffer = NULL;
    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
        NULL,
        GetLastError(),
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR)&lpBuffer,
        0, NULL);
    wprintf_s(L"\n[ERROR]%s\n", (WCHAR*)lpBuffer);
}