#pragma once
/*
// 共享变量使用前声明
HANDLE hSemap[2];                                       // 信号量句柄
WCHAR nameSemap[2][10] = {L"dbgSemap", L"dbgHeap"};     // 信号量名称
HANDLE hMap[2];                                         // 内存映射对象句柄
WCHAR nameShm[2][10] = {L"dbgShm", L"dgbHeap"};         // 共享内存名称
LPVOID pShm[2];                                         // 共享内存指针
func_info* shm;                                         // 函数信息结构体指针
heap_func_info* shm_heap;                               // 堆函数信息结构体指针
std::mutex mtx[2];                                      // 共享内存读写锁
*/

#ifndef DEALSHM_H
#define DEALSHM_H

#include <windows.h>
#include <iostream>
#include <mutex>

#define argValueSize 120
typedef struct shm_data {
    SYSTEMTIME st;                          // Hook处理时间戳
    int argNum;                             // 参数数量
    // argName首项为函数类型序号，argValue首项为函数名称
    char argName[10][30];                   // 参数名称
    char argValue[10][argValueSize];        // 参数值
}func_info;

typedef struct{
    SYSTEMTIME st;                          // Hook处理时间戳
    int argNum;                             // 参数数量
    // argName首项为函数类型序号，argValue首项为函数名称
    char argName[10][20];                   // 参数名称
    char argValue[10][12];        // 参数值
}heap_func_info;


// DealShm.cpp: 共享内存
void* InitShm(HANDLE& hSemaphore, HANDLE& hMap, LPVOID& pShm, WCHAR nameSemap[], WCHAR nameShm[]);
void* InitShm(HANDLE& hMap, LPVOID& pShm, WCHAR nameShm[]);
// 关闭共享内存, 防止内存泄漏
void DestoryShm(HANDLE& hMap, LPVOID& pShm);

#endif