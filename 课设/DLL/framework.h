﻿#pragma once

#ifndef FRAMEWORK_H
#define FRAMEWORK_H
#define WIN32_LEAN_AND_MEAN             // 从 Windows 头文件中排除极少使用的内容

// Windows 头文件
#include <windows.h>
#include "detours.h"
#include <stdio.h>
#include <string.h>
#include "stdarg.h"
#include <iostream>
#include <cstdio>
#include <WinSock2.h>
#include <Ws2tcpip.h>

#pragma comment(lib,"detours.lib")
#pragma comment (lib, "ws2_32.lib")  //加载 ws2_32.dll

DWORD WINAPI ThreadServer(LPVOID lpParam);
HANDLE ThreadCreate(char ThreadName[], LPTHREAD_START_ROUTINE ThreadFunc, LPVOID lpParam);
#endif