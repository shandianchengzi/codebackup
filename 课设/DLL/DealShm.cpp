#include "pch.h"
#include "DealShm.h"

void* InitShm(HANDLE& hSemaphore, HANDLE& hMap, LPVOID& pShm, WCHAR nameSemap[], WCHAR nameShm[])
{
    // 打开或创建信号量
    hSemaphore = OpenSemaphore(EVENT_ALL_ACCESS, FALSE, nameSemap);
    if (hSemaphore == NULL) {
        hSemaphore = CreateSemaphore(NULL, 0, 1, nameSemap);
        if (hSemaphore == NULL) {
            OutputDebugString(L"信号量创建失败！");
            exit(-1);
        }
    }
    // 打开或创建共享内存: Name:"dbgBuf", size:3000h
    hMap = OpenFileMapping(FILE_MAP_ALL_ACCESS, 0, nameShm);
    if (hMap == NULL) {
        hMap = CreateFileMapping(INVALID_HANDLE_VALUE, NULL,
            PAGE_READWRITE | SEC_COMMIT, 0, 0x3000, nameShm);
        if (hMap == NULL) {
            OutputDebugString(L"共享内存创建失败！");
            exit(-1);
        }
    }
    pShm = MapViewOfFile(hMap, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, 0);
    if (pShm == NULL)
    {
        OutputDebugString(L"获取共享内存映像失败！");
        exit(-1);
    }
    // 初始化共享内存内容为0
    memset(pShm, 0, sizeof(func_info));
    return pShm;
}

void* InitShm(HANDLE& hMap, LPVOID& pShm, WCHAR nameShm[])
{
    // 打开或创建共享内存: Name:"dbgBuf", size:3000h
    hMap = OpenFileMapping(FILE_MAP_ALL_ACCESS, 0, nameShm);
    if (hMap == NULL) {
        hMap = CreateFileMapping(INVALID_HANDLE_VALUE, NULL,
            PAGE_READWRITE | SEC_COMMIT, 0, 0x3000, nameShm);
        if (hMap == NULL) {
            OutputDebugString(L"共享内存创建失败！");
            exit(-1);
        }
    }
    pShm = MapViewOfFile(hMap, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, 0);
    if (pShm == NULL)
    {
        OutputDebugString(L"获取共享内存映像失败！");
        exit(-1);
    }
    // 初始化共享内存内容为0
    memset(pShm, 0, sizeof(func_info));
    return pShm;
}


//关闭共享内存, 防止内存泄漏
void DestoryShm(HANDLE& hMap, LPVOID& pShm)
{
    if (hMap != nullptr)
    {
        UnmapViewOfFile(pShm);      //解除文件映射
        CloseHandle(hMap);          //关闭句柄
    }
}

// 返回字符串中换行替换成原始字符，去掉转义效果(不改变原字符串)
char* Alter(char* tempStr, char* str)
{
    int i = 0, j = 0;
    doubleFan(str, tempStr, 100);
    strcpy_s(str, 100, tempStr);
    while (str[i] && j < argValueSize - 4)
    {
        if (str[i] == '\n')
        {
            tempStr[j++] = '\\';
            tempStr[j++] = 'n';
        }
        else if (str[i] != '\r')
        {
            tempStr[j++] = str[i];
        }
        i++;
    }
    // 限制tempStr的长度
    if (j == argValueSize - 4)
    {
        tempStr[j] = '.';
        tempStr[j + 1] = '.';
        tempStr[j + 2] = '.';
        tempStr[j + 3] = 0;

    }
    else
        tempStr[j] = str[i];
    return tempStr;
}

char* AlterRN(LPCSTR OriginStr)
{
    if (!OriginStr)return (char*)"";
    char tempStr[1000], str[1000];
    sprintf_s(tempStr, sizeof(tempStr), "%s", OriginStr);
    sprintf_s(str, sizeof(str), "%s", OriginStr);

    return Alter(tempStr, str);
}

char* AlterRN(LPCWSTR OriginStr)
{
    if (!OriginStr)return (char*)"";
    char tempStr[1000], str[1000];
    sprintf_s(tempStr, sizeof(tempStr), "%ws", OriginStr);
    sprintf_s(str, sizeof(str), "%ws", OriginStr);

    return Alter(tempStr, str);
}

void doubleFan(char* str, char * afterStr, char bufSize)
{
    int i = 0, j = 0;
    while (str[i] && j < argValueSize - 4) {
        if (str[i] == '\\') {
            afterStr[j++] = '\\';
        }
        afterStr[j++] = str[i++];
    }
    if (j == argValueSize - 4)
    {
        afterStr[j++] = '.';
        afterStr[j++] = '.';
        afterStr[j++] = '.';
    }
    afterStr[j] = 0;

}