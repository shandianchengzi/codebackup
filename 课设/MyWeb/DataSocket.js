
layui.use(['laydate', 'laypage', 'layer', 'table', 'carousel', 'upload', 'element'], function() {
    var laydate = layui.laydate //日期
    ,laypage = layui.laypage //分页
    ,layer = layui.layer //弹层
    ,table = layui.table //表格
    ,carousel = layui.carousel //轮播
    ,upload = layui.upload //上传
    ,element = layui.element; //元素操作 等等...

    /*layer弹出一个示例*/
    //layer.msg('Hello World');
  });

function b64Encode(str) {
    return btoa(unescape(encodeURIComponent(str)));
}

function b64Decode(str) {
    return decodeURIComponent(escape(atob(str)));
}

var ws;
var is_hooking=0;
$('#btn_start').click(function(){
    if ("WebSocket" in window) {
        console.log("start hooking...");
        if(!is_hooking)
        {
            ws = new WebSocket("ws://localhost:8092/");
            ws.binaryType = 'arraybuffer';
            ws.onopen = function () {
                // Web Socket 已连接上，使用 send() 方法发送数据
                is_hooking=1;
                ws.send(b64Encode("start hook"));
            };

            ws.onmessage = function (e) {
                //var res=JSON.parse(e.data);
                //console.log(res);
                //console.log(e.data);
                console.log(JSON.parse(b64Decode(e.data)));
                //console.log(e);

            };

            ws.onclose = function () {
                // 关闭 websocket
                is_hooking=0;
                alert("连接已关闭...");
            };
    }
    } else {
        // 浏览器不支持 WebSocket
        alert("您的浏览器不支持 WebSocket!");
    }
});



$('#btn_stop').click(function(){
    ws.send(b64Encode("stop hook"));
    ws.close();
});
